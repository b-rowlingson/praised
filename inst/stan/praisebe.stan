
data {
    int<lower=0> N;
    int<lower=0> M;
    int<lower=0, upper=1> ccflag[N];
    real<lower=0> d[N,M];
    real<lower=0> dmax;
}

parameters {
    real<lower=0> alpha;
    real<lower=0> beta;
    real<lower=0> delta;
}

model {
    real dterm;
    real lambda;
    delta ~ uniform(0, dmax);
    for(j in 1:N){
        dterm = 0;
        for(i in 1:M){
           dterm = dterm + exp(-((d[j,i]/delta)^2));
           }
        lambda = alpha + beta * dterm;
        ccflag[j] ~ bernoulli(1-exp(-lambda));
    }
}


