praise_ll <- function(xypts, ccflag, xysrc){

    xypts = sp::SpatialPoints(xypts)
    xysrc = sp::SpatialPoints(xysrc)
    d = rgeos::gDistance(xypts, xysrc, byid=TRUE)
    pf = pfunc(d)
    ll = function(abd){
        p = pf(abd)
        sum(log(ifelse(ccflag==1,p, 1-p)))
    }

    ll
    
}

praise_p <- function(r, xysrc){
    xypts = sp::SpatialPoints(sp::coordinates(r))
    xysrc = sp::SpatialPoints(xysrc)
    d = rgeos::gDistance(xypts, xysrc, byid=TRUE)
    pf = pfunc(d)
    p = function(abd){
        pv = pf(abd)
        r[] = pv
        r
    }
    p
}

pfunc = function(d){
    p = function(abd){
        alpha=abd[1]
        beta=abd[2]
        delta=abd[3]
        expd = exp(-((d/delta)^2))
        lambda = alpha + beta * apply(expd,2,sum)
        1 - exp(-lambda)
    }
    p
}


fit_praise <- function(pts, ccflag, srcs, start){

    fn = praise_ll(pts, ccflag, srcs)

    op = optim(start,fn,hessian=TRUE,control=list(fnscale=-1))

    se = sqrt(diag(solve(-op$hessian)))
    res = data.frame(estimate = op$par, se=se)
    rownames(res)=c("alpha","beta","delta")
    op$ll = fn
    ncase = sum(ccflag)
    npts = length(ccflag)
    pcase = ncase/npts
    op$log_lik0 = ncase*log(pcase) + (npts-ncase)*log(1-pcase)
    op$res = res
    class(op)="praised"
    op
}

print.praised <- function(x,...){
    print(x$res)
    cat("\nLog-likelihood      = ",x$value,"\n")
    cat("Null Log-likelihood = ",x$log_lik0,"\n")
}

praise_stan <- function(xypts, ccflag, xysrc, ...){
    N = nrow(xypts)
    M = nrow(xysrc)
    xypts = sp::SpatialPoints(xypts)
    xysrc = sp::SpatialPoints(xysrc)
    d = rgeos::gDistance(xysrc, xypts, byid=TRUE)

    data = list(N=N, M=M, d=d, ccflag=ccflag, dmax=max(d))
    sfile = system.file("stan/praisebe.stan", package="praised")
    stanfit = rstan::stan(file = sfile, data=data,  ...)
    stanfit
}
