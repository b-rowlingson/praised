# Raised Incidence Modelling

```
farms = structure(list(x = c(1.43120877917955, 1.43511569196245, 1.51520740401193, 
1.65976317697928, 1.85901572890726, 1.33158250321557, 1.44097606113681, 
1.10498156180728, 1.18507327385676), y = c(0.435442032821599, 
0.513945107301381, 0.496499979639207, 0.302422934397524, 0.923905607362463, 
0.753815612656269, 0.712383434458607, 0.328590625890785, 0.173765117888993
)), .Names = c("x", "y"), row.names = c(NA, -9L), class = "data.frame")

r = raster(xmn=1,xmx=2,ymn=0,ymx=1,nrow=100, ncol=100)

rp = praise_p(r, farms)(c(.1,1.9,.05))

pts = data.frame(x=runif(1000,1,2),y=runif(1000,0,1))

ccflag = rbinom(nrow(pts), size=1, prob=extract(rp,pts))

fit = fit_praise(pts, ccflag, farms, c(.1,1.9,.05))

rp_op= praise_p(r, farms)(fit$par)
```

Or with stan:

```
stanfit = praise_stan(pts, ccflag, farms)
```
